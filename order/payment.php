<?php
if ($_POST) {
    $_merchantID = '572a31a33b1eaf61438b456f';

    $paymentStatus = $_POST['ik_inv_st'];
    $paymentID = $_POST['ik_pm_no'];
    $paymentDate = $_POST['ik_inv_crt'];
    $paymentSum = $_POST['ik_am'];
    $clientEmail = $_POST['ik_x_email'];

    //Here payment check
    if (($paymentStatus != 'success') || !$paymentID || ($_merchantID != $_POST['ik_co_id'])) die();
    //

    $orderMail = '';
    $clientMail = '';

    // $orderMail .= "<h3>Детали заказа:</h3><strong>Кол-во черных блокнотов:</strong> {$_POST['ik_x_black_quantity']}<br>";
    // $orderMail .= "<strong>Кол-во желтых блокнотов:</strong> {$_POST['ik_x_yellow_quantity']}<br>";
    // $orderMail .= "<strong>Заказчик:</strong> {$_POST['ik_x_contact_info']}<br>";
    // $orderMail .= "<strong>Контактный телефон:</strong> {$_POST['ik_x_contact_phone']}<br>";
    // $orderMail .= "<strong>Контактный email:</strong> {$_POST['ik_x_email']}<br>";
    // $orderMail .= "<strong>Тип доставки:</strong> {$_POST['ik_x_delivery_type']}<br>";
    // if ($_POST['ik_x_delivery_type'] == 'Доставка почтой') {
    //     $orderMail .= "<strong>Страна:</strong> {$_POST['ik_x_delivery_country']}<br>";
    //     $orderMail .= "<strong>Город:</strong> {$_POST['ik_x_delivery_city']}<br>";
    //     $orderMail .= "<strong>Адрес:</strong> {$_POST['ik_x_delivery_address']}<br>";
    // }
    // $orderMail .= "<strong>Комментарий к заказу:</strong> {$_POST['ik_x_comment_message']}<br>";
    $orderMail .=  "<h3>Детали оплаты:</h3><strong>ID платежа:</strong>$paymentID<br><strong>Сумма платежа:</strong>$paymentSum RUB<br><strong>Дата и время платежа:</strong>$paymentDate";

    $clientMail = "<h2>Заказ №$paymentID успешно оплачен и принят в обработку. В ближайшее время с Вами свяжется менеджер.</h1>";
    $clientMail .= "<h3>Сумма заказа: $paymentSum RUB</h3>";

    // Prepare e-mail
    require_once 'phpmailer/PHPMailerAutoload.php';

    // Lopart email
    $mail = new PHPMailer;
    $mail->CharSet = 'UTF-8';
    $mail->setFrom('webnote@lopart.by', 'Команда Lopart');
    $mail->addAddress('webnote@lopart.by');
    $mail->addAddress('leznii@gmail.com');
    $mail->addReplyTo('webnote@lopart.by', 'Команда Lopart');
    $mail->isHTML(true);

    $mail->Subject = 'Новый заказ Webnote';
    $mail->Body = $orderMail;
    $mail->send();

    // Client email
    $cmail = new PHPMailer;
    $cmail->CharSet = 'UTF-8';
    $cmail->setFrom('webnote@lopart.by', 'Команда Lopart');
    $cmail->addAddress($clientEmail);
    $cmail->addReplyTo('webnote@lopart.by', 'Команда Lopart');
    $cmail->isHTML(true);

    $cmail->Subject = 'Подтверждение заказа Webnote';
    $cmail->Body = $clientMail;
    $cmail->send();

    die();
}
?>
