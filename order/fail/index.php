<!DOCTYPE html>
<html lang="ru" prefix="http://ogp.me/ns#">
<head>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=1030"/>
    <meta name="dns-prefetch" content="//fonts.googleapis.com"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="interkassa-verification" content="bc91c9baf893728fda470dbadf7d87ce" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <!--[if lt IE 9]>
  	<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<![endif]-->
    <link rel="stylesheet" href="../../css/style.min.css">
	<title>WEBNOTE &ndash; заказ в обработке</title>
</head>
<body class="order-page">
    <section class="order order-status">
        <a href="http://lopart.by/webnote"><img src="../../img/logo.png" class="order-logo"></a>
        <div class="container">
            <hgroup>
                <h2 class="order-form-title">Оплата не проведена</h2>
                <h5 class="order-form-subtitle">
                    К сожалению, мы не смогли провести Ваш платеж, попробуйте еще раз.<br>
                    Если ошибка появится снова - напишите на <a href="mailto:webnote@lopart.by" class="webnote-news">webnote@lopart.by</a>.<br>
                    Приносим извинения за данную ситуацию, нам очень жаль, честно :(
                </h5>
            </hgroup>
            <a href="http://vk.com/lopart_webnote" target="_blank" class="webnote-news">Последние новости</a>
        </div>
    </section>
    <section class="support">
        <div class="container">
            <p>Мы всегда рады Вашим сообщениям! Отправляйте нам свои вопросы или предложения. Мы ответим всем.</p>
            Email: <a href="mailto:webnote@lopart.by" class="webnote-news">webnote@lopart.by</a><br>
            Skype: <a href="skype:artur_chellos?chat" class="webnote-news">artur_chellos</a>
        </div>
    </section>
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="../../js/main.min.js"></script>
</body>
</html>
