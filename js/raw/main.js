$ = jQuery;

$(document).on('ready', ready);
$(window).on('scroll', scrolling);
$(window).on('resize', resizing);

$('.popup').click(function() {

    var target = $(this).attr('href');
    $.magnificPopup.open({
        items: {
            src: target,
            type: 'inline'
        }
    });

    return false;
});

// $('.order-form-delivery input[type="radio"] + label').click(function() {
//     var type = $(this).prev('input[type="radio"]').attr('id');
//     $('.order-form-delivery-info').removeClass('active');
//     $('.order-form-delivery-info[data-for="' + type + '"]').addClass('active');
// });

// var course = parseInt($('[name="course"]').val());
// var cost = parseInt($('[name="price"]').val());
// var price = 12;
// var pricePay = 792;
// var priceDelivery = 0;
// var desc = '';
//
// var specialOrderLeft = 0;
// var orderLeft = 0;

// $('#black-color-quantity, #yellow-color-quantity').on('change keyup', function() {
//
//     if (parseInt($(this).val()) <= 0 || $(this).val() == '') {
//         $(this).val(0);
//     }
//
//     calc();
// });

// $('[name="ik_cli"]').on('change keyup', function() {
//     $('[name="ik_x_email"]').val($('[name="ik_cli"]').val());
// });

// $('.order-form-delivery > ul label').on('click', function() {
//
//     priceDelivery = 0;
//
//     if ($(this).prev('[type="radio"]').val() == 'Доставка почтой') {
//         priceDelivery = 4;
//     }
//
//     if ($(this).prev('[type="radio"]').val() == 'Доставка по Беларуси') {
//         priceDelivery = 1;
//     }
//
//     calc();
//  });

// $('#delivery-country').on('change', function() {
//
//     priceDelivery = parseInt($('#delivery-country').find('option:selected').attr('data-cost'));
//     calc();
// });

$('.webnote-info-arrow').click(function() {

    $('html, body').animate({
        scrollTop: $('.preorder-special').offset().top
    }, 700);
});

function ready() {

    specialOrderLeft = parseInt($('[name="order-special-left"]').val());
    orderLeft = parseInt($('[name="order-left"]').val());

    $('.preorder-special-info span').text(specialOrderLeft);
    $('.preorder-quantity span').text(orderLeft);

    if (specialOrderLeft == 0) {
        $('.preorder-special-get').attr('href', '#preorder-special-stopped');
    }
    if (orderLeft == 0) {
        $('#preorder .preorder-get').attr('href', '#preorder-stopped');
    }

    $('.advantages-slider').slick({
        arrows: false,
        dots: true,
        slidesToShow: 1
    });

    $('.preorder-slider').slick({
        arrows: false,
        dots: true,
        slidesToShow: 1
    });

    $('.webnote-pages-slider').slick({
        arrows: false,
        dots: true,
        slidesToShow: 5,
        slidesToScroll: 5
    });

    $('.webnote-pages-slider').magnificPopup({
		delegate: 'a:not(.slick-cloned)',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1]
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
		}
	});

    //$('[name="ik_pm_no"]').val(Math.floor(Date.now() / 1000));
}

function scrolling() {
}

function resizing() {}

// function calc() {
//
//     var yellowQuantity = parseInt($('#yellow-color-quantity').val());
//     var blackQuantity = parseInt($('#black-color-quantity').val());
//     price = (yellowQuantity + blackQuantity) * cost + priceDelivery;
//     pricePay = price * course;
//
//     $('.order-form-checkout-price span').text('$' + price);
//     $('[name="ik_am"]').val(pricePay);
//     if (pricePay < 528) {
//         $('[type="submit"]').attr('disabled', 'disabled');
//     } else {
//         $('[type="submit"]').removeAttr('disabled');
//     }
// }
